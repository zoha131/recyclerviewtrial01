package io.github.zoha131.recyclerviewtrial01;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import io.github.zoha131.recyclerviewtrial01.data.Product;
import io.github.zoha131.recyclerviewtrial01.data.Products;

public class MainActivity extends AppCompatActivity implements MyClickListener {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Products products = new Products();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyAdapter(products.PRODUCTS, this));
        recyclerView.addItemDecoration(new MyItemDecorator(30));
    }

    @Override
    public void onImageClicked(Product product) {
        Toast.makeText(this, product.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClicked(Product product) {
        Toast.makeText(this, product.getNumberRatingsString(), Toast.LENGTH_SHORT).show();
    }
}
