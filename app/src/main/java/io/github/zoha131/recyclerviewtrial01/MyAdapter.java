package io.github.zoha131.recyclerviewtrial01;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Locale;

import io.github.zoha131.recyclerviewtrial01.data.Product;

/**
 * Created by abirh on 21-Mar-18.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    Product[] data;
    MyClickListener myClickListener;

    public MyAdapter(Product[] data, MyClickListener myClickListener) {
        this.data = data;
        this.myClickListener = myClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view, myClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = data[position];
        holder.setProduct(product);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, desc, price;
        Product product;

        public ViewHolder(View itemView, final MyClickListener myClickListener) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageView);
            title = (TextView) itemView.findViewById(R.id.titleTxt);
            desc = (TextView) itemView.findViewById(R.id.descText);
            price = (TextView) itemView.findViewById(R.id.price);

            image.setOnClickListener(view -> myClickListener.onImageClicked(product));
            itemView.setOnClickListener(view -> myClickListener.onItemClicked(product));
        }

        public void setProduct(Product product){
            this.product = product;

            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background);
            Glide.with(image.getContext())
                    .setDefaultRequestOptions(requestOptions)
                    .load(product.getImage())
                    .into(image);

            //holder.image.setImageResource(product.getImage());
            title.setText(product.getTitle());
            desc.setText(product.getDescription());
            price.setText(String.format(Locale.US, "%.2f", product.getPrice().floatValue()));
        }

    }
}
