package io.github.zoha131.recyclerviewtrial01;

import io.github.zoha131.recyclerviewtrial01.data.Product;

public interface MyClickListener {
    void onImageClicked(Product product);
    void onItemClicked(Product product);
}
