package io.github.zoha131.recyclerviewtrial01;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by abirh on 26-Mar-18.
 */

public class MyItemDecorator extends RecyclerView.ItemDecoration {
    private int mItemOffset;

    public MyItemDecorator(int mItemOffset) {
        this.mItemOffset = mItemOffset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
    }
}
